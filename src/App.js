import React from 'react';
import logo from './logo.svg';
import './App.css';
import Header from '../src/Components/Header';
import UserList from '../src/Components/UserList';
import { createStyles, Theme, makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) =>
  createStyles({
    container: {
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
    },
  }),
);

function App() {
  const classes = useStyles();
  return (
    <div className="App">
      <div className={classes.container}>
        <Header title="USER MANAGEMENT" />
        <UserList />
      </div>
    </div>
  );
}

export default App;

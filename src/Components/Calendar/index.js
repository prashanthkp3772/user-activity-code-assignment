import React from 'react';
import { createStyles, makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';

const useStyles = makeStyles((theme) =>
  createStyles({
    container: {
      display: 'flex',
      flexWrap: 'wrap',
      marginBottom: '1rem',
    },
    textField: {
      marginLeft: theme.spacing(1),
      marginRight: theme.spacing(1),
      width: 300,
    },
  }),
);

export default function DateAndTimePickers(props) {
  const classes = useStyles();

  const today = () => {
    var today = new Date();
    var date =
      today.getFullYear() + '-' + ('0' + (today.getMonth() + 1)).slice(-2) + '-' + ('0' + today.getDate()).slice(-2);
    var time = ('0' + today.getHours()).slice(-2) + ':' + ('0' + today.getMinutes()).slice(-2);
    var dateTime = `${date}T${time}`;
    return dateTime;
  };

  return (
    <form className={classes.container} noValidate>
      <TextField
        id="datetime-local"
        label={props.title}
        type="datetime-local"
        defaultValue={today()}
        className={classes.textField}
        InputLabelProps={{
          shrink: true,
        }}
        onChange={(event) => props.updateDate(new Date(event.target.value))}
      />
    </form>
  );
}

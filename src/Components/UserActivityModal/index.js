import React, { useState } from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import SvgIcon from '@material-ui/core/SvgIcon';
import Divider from '@material-ui/core/Divider';

import Calendar from '../Calendar';

function UserIcon() {
  return (
    <SvgIcon style={{ color: '#1e88e5' }}>
      <path d="M3 5v14c0 1.1.89 2 2 2h14c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2H5c-1.11 0-2 .9-2 2zm12 4c0 1.66-1.34 3-3 3s-3-1.34-3-3 1.34-3 3-3 3 1.34 3 3zm-9 8c0-2 4-3.1 6-3.1s6 1.1 6 3.1v1H6v-1z" />
    </SvgIcon>
  );
}

export default function UserActivityModal(props) {
  // formating  the Date
  const [fromDate, setFormDate] = useState(new Date());
  const [toDate, setToDate] = useState(new Date());

  const processDates = (userData) => {
    let activityDateArray = [];
    userData.user['activity_periods'].map((activity) => {
      let activities = {};
      let startDate = activity['start_time'].toUpperCase();
      let endDate = activity['end_time'].toUpperCase();

      let startHour = '';
      let endHour = '';

      let splitStartTime = activity['start_time'].split(' ');
      let splitEndTime = activity['end_time'].split(' ');

      let startDateSplitArray = [];

      splitStartTime.map((element) => {
        if (element && !element.includes(':')) startDateSplitArray.push(element);
        else if (element.includes(':')) startHour = element.split(':')[0];
      });

      let endDateSplitArray = [];

      splitEndTime.map((element) => {
        if (element && !element.includes(':')) endDateSplitArray.push(element);
        else if (element.includes(':')) endHour = element.split(':')[0];
      });

      if (startDate.includes('AM')) {
        activities['start_time'] = startDate.split('AM')[0];
      } else if (startDate.includes('PM')) {
        activities['start_time'] =
          startDateSplitArray.join('-') + '  ' + (1 * startHour + 12) + ':' + startDate.split('PM')[0].split(':')[1];
      }

      if (endDate.includes('AM')) {
        activities['end_time'] = endDate.split('AM')[0];
      } else if (endDate.includes('PM')) {
        activities['end_time'] =
          endDateSplitArray.join('-') + '  ' + (1 * endHour + 12) + ':' + endDate.split('PM')[0].split(':')[1];
      }

      activityDateArray.push(activities);
    });

    return activityDateArray;
  };

  const getListItems = () => {
    const listItems = props.user['activity_periods'].map((period, index) => {
      // get any record  which has start_time/end_time between selected start and end time
      if (
        (new Date(userActivity[index]['start_time']).getTime() >= fromDate.getTime() &&
          new Date(userActivity[index]['start_time']).getTime() <= toDate.getTime()) ||
        (new Date(userActivity[index]['end_time']).getTime() >= fromDate.getTime() &&
          new Date(userActivity[index]['end_time']).getTime() <= toDate.getTime())
      ) {
        return (
          <div>
            <ListItem button key={index} onClick={(event) => {}}>
              <ListItemIcon>
                <UserIcon />
              </ListItemIcon>
              <ListItemText primary={`${period.start_time} <-> ${period.end_time}`} />
            </ListItem>
            <Divider />
          </div>
        );
      }
    });
    return listItems;
  };

  const userActivity = processDates(props);

  return (
    <div>
      <Dialog
        open={props.show}
        fullWidth={true}
        maxWidth="md"
        keepMounted
        onClose={props.handleClose}
        aria-labelledby="alert-dialog-slide-title"
        aria-describedby="alert-dialog-slide-description"
      >
        <DialogTitle
          id="alert-dialog-slide-title"
          style={{ color: '#1e88e5' }}
        >{`${props.user.real_name}'s Activity`}</DialogTitle>
        <DialogContent
          style={{
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
            width: '100%',
          }}
        >
          <div
            style={{
              display: 'flex',
              flexDirection: 'row',
              alignItems: 'center',
              width: '100%',
              justifyContent: 'space-around',
            }}
          >
            <Calendar updateDate={setFormDate} style={{ width: '100%' }} title="START TIME" />
            <Calendar updateDate={setToDate} title="END TIME" />
          </div>

          <List component="nav" aria-label="main mailbox folders" style={{ width: '80%' }}>
            {getListItems()}
          </List>
        </DialogContent>
        <DialogActions>
          <Button onClick={props.handleClose} color="primary">
            CLOSE
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}

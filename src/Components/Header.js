import React, { useState, useEffect } from 'react';
import './header.css';

const Header = (props) => {
  return (
    <div className="header">
      <h1 style={{ color: 'white' }}>{props.title}</h1>
    </div>
  );
};

export default Header;

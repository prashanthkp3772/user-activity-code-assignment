import React, { useEffect, useState } from 'react';
import { createStyles, Theme, makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem, { ListItemProps } from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Divider from '@material-ui/core/Divider';
import SvgIcon from '@material-ui/core/SvgIcon';
import { Typography } from '@material-ui/core';
import axios from 'axios';
import UserActivityModal from '../UserActivityModal';

const useStyles = makeStyles(() =>
  createStyles({
    root: {
      width: '100%',
      // backgroundColor: '#e3f2fd',
      marginTop: '1rem',
      maxWidth: '50rem',
      boxShadow: '0px 2px 3px #1e88e5',
      borderRadius: '0.5rem',
    },
    titleContainer: {
      width: '100%',
      height: '3rem',
      backgroundColor: '#e3f2fd',
      borderTopLeftRadius: '0.5rem',
      borderTopRightRadius: '0.5rem',
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
    },
  }),
);

function UserIcon() {
  return (
    <SvgIcon style={{ color: '#1e88e5' }}>
      <path d="M3 5v14c0 1.1.89 2 2 2h14c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2H5c-1.11 0-2 .9-2 2zm12 4c0 1.66-1.34 3-3 3s-3-1.34-3-3 1.34-3 3-3 3 1.34 3 3zm-9 8c0-2 4-3.1 6-3.1s6 1.1 6 3.1v1H6v-1z" />
    </SvgIcon>
  );
}

export default function SimpleList() {
  const [users, setUsers] = useState([]);
  const [userSelected, setUserSelected] = useState(false);
  const [selectUserIndex, setSelectUserIndex] = useState(null);

  const handleModalClose = () => setUserSelected(false);
  const classes = useStyles();
  useEffect(() => {
    axios.get(`https://target100.s3.ap-south-1.amazonaws.com/target100/user-managerment/userList.json`).then((res) => {
      setUsers(res.data['members']);
    });
  }, []);

  return (
    <div className={classes.root}>
      <div className={classes.titleContainer}>
        <Typography variant="h6" style={{ color: '#1e88e5' }}>
          USERS LIST
        </Typography>
      </div>

      <List component="nav" aria-label="main mailbox folders">
        {users.map((user, index) => (
          <ListItem
            button
            key={index}
            onClick={(event) => {
              setUserSelected(true);
              setSelectUserIndex(index);
            }}
          >
            <ListItemIcon>
              <UserIcon />
            </ListItemIcon>
            <ListItemText primary={user['real_name']} />
          </ListItem>
        ))}
      </List>
      {selectUserIndex !== null && (
        <UserActivityModal show={userSelected} handleClose={handleModalClose} user={users[selectUserIndex]} />
      )}
    </div>
  );
}
